# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the InstaGENI library.
import geni.rspec.igext as ig
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

pc.defineParameter("vmType","VM Type",portal.ParameterType.STRING,"Xen",
                   [("Docker","Docker"),("Xen","Xen")],
                   longDescription="Choose a VM type (NB: if you force a host image, ensure it is compatible with this choice!)")
pc.defineParameter("vmsPerHost","VMs per Host",portal.ParameterType.INTEGER,1,
                   longDescription="Number of VMs per physical host.")
pc.defineParameter("hosts","Physical Hosts",portal.ParameterType.INTEGER,1,
                   longDescription="Number of physical hosts.  If 0, your VMs will be allocated in shared mode instead of dedicated.")
pc.defineParameter("hostType","Host Node Type",portal.ParameterType.NODETYPE,"",
                   longDescription="Node type of physical host nodes.")
pc.defineParameter("coresPerVM","Cores per VM",portal.ParameterType.INTEGER,0,
                   longDescription="Number of cores each VM will get.")
pc.defineParameter("ramPerVM","RAM per VM",portal.ParameterType.INTEGER,0,
                   longDescription="Amount of RAM each VM will get.")
pc.defineParameter("forceXenHVM","Force Xen HVM",portal.ParameterType.BOOLEAN,False,
                   longDescription="Force Xen VM to be created in HVM mode.")
pc.defineParameter("forceXenPVH","Force Xen PVH",portal.ParameterType.BOOLEAN,False,
                   longDescription="Force Xen VM to be created in PVH mode.")
pc.defineParameter("dockerExternalImages","Images are External Docker",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription='If creating Docker VMs, the specified VM Images are all external Docker images, and a placeholder Cloudlab redirect image will point to them.')
pc.defineParameter("dockerImageFromDockerfiles","Images are Dockerfiles",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription='If creating Docker VMs, the specified images are all Dockerfile URLs, not actual images; we will create and use them on-demand.')
pc.defineParameter("vmImages","VM Images",portal.ParameterType.STRING,
                   'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD',
                   longDescription="A list of images your VMs will run, separated by whitespace.  If more than one, we will alternate through the list as we create VMs.")
pc.defineParameter("storageDriver","Storage Driver",
                   portal.ParameterType.STRING,'',
                   [('',''),('devicemapper','devicemapper'),
                    ('overlay2','overlay2'),('aufs','aufs')],
                   longDescription="The Docker storage driver the host machines will use.")
pc.defineParameter("dockerCmd","Per-node Docker Command",
                   portal.ParameterType.STRING,"",
                   longDescription="As specified in the Docker documentation, this value will either be concatenated with the value of entrypoint in the docker image file; or it will replace it.  Note: we emulate entrypoint/command as a boot service, because we run a real init as the entrypoint.")
pc.defineParameter("dockerEnv","Per-node Docker Environment",
                   portal.ParameterType.STRING,"",
                   longDescription="These environment variables will be placed in a shell script and source by the entrypoint/command emulation.  Shells accept whitespace-separated variable assigment statements; you could also separate them by semicolons if you prefer.  Either way, this value will be placed into a one-line file that will be sourced by another shell script.")
pc.defineParameter("dockerTbAugmentation","Docker TbAugmentation Level",
                   portal.ParameterType.STRING,'default',
                   [('default','Default'),('none','None'),('basic','Basic'),
                    ('core','Core'),('buildenv','BuildEnv'),('full','Full')],
                   longDescription="The requested tbAugmentation level; may be either full, buildenv, core, basic, none.  To augment a Docker image is to take the image and install some or all of the Emulab clientside and dependencies, and other generally useful networking packages, before starting up the nodes that use it, so that it works seamlessly in testbeds based on Emulab.")
pc.defineParameter("dockerTbAugmentationUpdate","Update Docker TbAugmentation",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription="If the image is already augmented, re-augment with the current source tree.")
pc.defineParameter("dockerSshStyle","Docker SSH Style",
                   portal.ParameterType.STRING,'default',
                   [('default','Default'),('direct','Direct'),('exec','Exec')],
                   longDescription="Specify what happens when you ssh to your node; may be direct or exec.  If your container is augmented > basic, and you do not specify this, it defaults to direct.  If your container is not augmented to that level and you do not specify this, it defaults to exec.  direct means that the container is running an sshd inside, and an incoming ssh connection will be handled by the container.  exec means that when you connection, sshd will exec a shell inside your container.  You can change that shell by specifying the docker_exec_shell value.")
pc.defineParameter("dockerExecShell","Docker Exec Shell",
                   portal.ParameterType.STRING,'',
                   longDescription="The shell to run if your Docker ssh_style is direct; otherwise ignored")
pc.defineParameter("startupCmd","VM Startup Command",portal.ParameterType.STRING,"",
                   longDescription="A command each VM will run on startup.")
pc.defineParameter("hostImage","Host Image",portal.ParameterType.STRING,'',
                   longDescription="The image your VM host nodes will run (ensure compat with VM Type choice!)")
pc.defineParameter("clientsideUpdate","Update Clientside",
                   portal.ParameterType.STRING,'no',
                   [('no','No'),('yes','Yes'),('each','Each Boot')],
                   longDescription="If set to Yes, the clientside will be updated on first boot for each vhost prior to booting vnodes.  If set to Each Boot, the clientside will be updated for each boot for each vhost.")
pc.defineParameter("clientsideUpdateRepo","Clientside Update Source Repo",
                   portal.ParameterType.STRING,"",
                   longDescription="If set, the updater will clone this git repo; default is https://gitlab.flux.utah.edu/emulab/emulab-devel.git")
pc.defineParameter("clientsideUpdateRef","Clientside Update Source Ref",
                   portal.ParameterType.STRING,"",
                   longDescription="If set, the updater will checkout this branch or tag instead of master; note that it must be a branch or tag, so that we can see its latest commit hash via ls-remote.")
pc.defineParameter("numberLans","Number of LANs",portal.ParameterType.INTEGER,0,
                   longDescription="Number of LANs to which each VM is connected.")
pc.defineParameter("buildSnakeLink","Build Snake of Links",portal.ParameterType.BOOLEAN,False,
                   longDescription="Build a snake of links from VM to VM.")
pc.defineParameter("linkSpeed", "LAN/Link Speed",portal.ParameterType.INTEGER, 1000000,
                   [(0,"Any"),(100000,"100Mb/s"),(1000000,"1Gb/s"),(10000000,"10Gb/s")],
                   longDescription="A specific link speed to use for each LAN and link.")
pc.defineParameter("linkLatency", "LAN/Link Latency",portal.ParameterType.LATENCY, 0,
                   longDescription="A specific latency to use for each LAN and link.")
#pc.defineParameter("linkLoss", "LAN/Link Loss",portal.ParameterType.FLOAT, 0.0,
#                   longDescription="A specific loss rate to use for each LAN and link.")
pc.defineParameter("multiplex", "Multiplex Networks",portal.ParameterType.BOOLEAN, True,
                   longDescription="Multiplex all LANs and links.")
pc.defineParameter("bestEffort", "Best Effort Link Bandwidth",portal.ParameterType.BOOLEAN, True,
                   longDescription="Do not require guaranteed bandwidth throughout switch fabric.")
pc.defineParameter("trivialOk", "Trivial Links Ok",portal.ParameterType.BOOLEAN, True,
                   longDescription="Maybe use trivial links.")
pc.defineParameter("routableIPChoice","Routable IPs",portal.ParameterType.STRING,
                   "none",[("none","None"),("first","First Node"),("all","All Nodes")],
                   longDescription="VMs for which to request a public control net IP.")
#pc.defineParameter("forceVlanLinkType", "Force VLAN Link Type",portal.ParameterType.BOOLEAN, False,
#                   longDescription="Force the type of each LAN/Link to vlan.")
pc.defineParameter("ipAllocationStrategy","IP Addressing",
                   portal.ParameterType.STRING,"script",[("cloudlab","CloudLab"),("script","This Script")],
                   longDescription="Either let CloudLab auto-generate IP addresses for the nodes in your networks, or let this script generate them.  If you include nodes at multiple sites, you must choose this script!  The default is this script, because the subnets CloudLab generates for flat networks are sized according to the number of physical nodes in your topology.  If the script IP address generation is buggy or otherwise insufficient, you can fall back to CloudLab and see if that improves things.")
pc.defineParameter("ipAllocationUsesCIDR","Script IP Addressing Uses CIDR",
                   portal.ParameterType.BOOLEAN,False,
                   longDescription="If this script is generating the IP addresses for your nodes, and this button is checked, we will generate a minimal set of subnets that are just large enough to contain the hosts in each subnet.  If it is not checked, we will generate class B/C subnets inside the 10.0.0.0/8 class A unrouted subnet, for each separate link/lan.")
pc.defineParameter("tempBlockstoreMountPoint", "Temporary Filesystem Mount Point",
                   portal.ParameterType.STRING,"",advanced=True,
                   longDescription="Mounts an ephemeral, temporary filesystem at this mount point, on the nodes which you specify below.  If you specify no nodes, and specify a mount point here, all nodes will get a temp filesystem.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).")
pc.defineParameter("tempBlockstoreSize", "Temporary Filesystem Size",
                   portal.ParameterType.INTEGER, 0,advanced=True,
                   longDescription="The necessary space in GB to reserve for your temporary filesystem.")
pc.defineParameter("tempBlockstoreMountNodes", "Temporary Filesystem Mount Node(s)",
                   portal.ParameterType.STRING,"",advanced=True,
                   longDescription="The node(s) on which you want a temporary filesystem created; space-separated for more than one.  Leave blank if you want all nodes to have a temp filesystem.")
pc.defineParameter("blockstoreURN", "Remote Dataset URN",
                   portal.ParameterType.STRING, "",advanced=True,
                   longDescription="The URN of an *existing* remote dataset (a remote block store) that you want attached to the node you specified (defaults to the first node created).  The block store must exist at the cluster at which you instantiate the profile!")
pc.defineParameter("blockstoreMountNode", "Remote Dataset Mount Node",
                   portal.ParameterType.STRING, "",advanced=True,
                   longDescription="The node on which you want your remote block store mounted; defaults to the first node created.")
pc.defineParameter("blockstoreMountPoint", "Remote Dataset Mount Point",
                   portal.ParameterType.STRING, "/dataset",advanced=True,
                   longDescription="The mount point at which you want your remote dataset mounted.  Be careful where you mount it -- something might already be there (i.e., /storage is already taken).  Note also that this option requires a network interface, because it creates a link between the dataset and the node where the dataset is available.  Thus, just as for creating extra LANs, you might need to select the Multiplex Flat Networks option, which will also multiplex the blockstore link here.")
pc.defineParameter("blockstoreReadOnly", "Mount Remote Dataset Read-only",
                   portal.ParameterType.BOOLEAN, True,advanced=True,
                   longDescription="Mount the remote dataset in read-only mode.")
pc.defineParameter(
    "createSharedVlan","Create Shared VLAN",
    portal.ParameterType.BOOLEAN,False,
    longDescription="Create a new shared VLAN with the name above, and connect the first node to it.",
    advanced=True)
pc.defineParameter(
    "connectSharedVlan","Shared VLAN Name",
    portal.ParameterType.STRING,"",
    longDescription="Connect `node-0` to a shared VLAN.  This allows your O-RAN experiment to connect to another experiment.  If the shared VLAN does not yet exist (e.g. was not manually created for you by an administrator, or created in another experiment), enable the next option to create it.",
    advanced=True)
pc.defineParameter(
    "sharedVlanAddress","Shared VLAN IP Address",
    portal.ParameterType.STRING,"10.254.254.254/255.255.255.0",
    longDescription="Set the IP address and subnet mask for the shared VLAN interface.  Make sure you choose an unused address within the subnet of an existing shared vlan!  Also ensure that you specify the subnet mask as a dotted quad.",
    advanced=True)

params = pc.bindParameters()

if params.hosts < 0:
    pc.reportError(portal.ParameterError("Must specify 0 or more physical hosts",['hosts']))
if params.vmsPerHost < 1:
    pc.reportError(portal.ParameterError("Must specify at least one VM per host",['vmsPerHost']))
if params.coresPerVM < 0:
    pc.reportError(portal.ParameterError("Must specify at least one core per VM",['coresPerVM']))
if params.ramPerVM < 0:
    pc.reportError(portal.ParameterError("Must specify at least one core per VM",['ramPerVM']))

#
# Handle temp blockstore param.  Note that we do not generate errors for
# non-existent nodes!
#
tempBSNodes = []
if params.tempBlockstoreMountPoint != "":
    if params.tempBlockstoreMountNodes:
        tempBSNodes = params.tempBlockstoreMountNodes.split()
    if params.tempBlockstoreSize <= 0:
        perr = portal.ParameterError("Your temporary filesystems must have size > 0!",
                                     ['tempBlockstoreSize'])
        pc.reportError(perr)
        pc.verifyParameters()
    pass

# Handle shared vlan address param.
(sharedVlanAddress,sharedVlanNetmask) = (None,None)
if params.sharedVlanAddress:
    aa = params.sharedVlanAddress.split('/')
    if len(aa) != 2:
        perr = portal.ParameterError(
            "Invalid shared VLAN address!",
            ['sharedVlanAddress'])
        pc.reportError(perr)
        pc.verifyParameters()
    else:
        (sharedVlanAddress,sharedVlanNetmask) = (aa[0],aa[1])

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

tour = ig.Tour()
tour.Description(ig.Tour.TEXT,"Instantiate one or many dedicated VMs with lots of potential parameters.")
request.addTour(tour)

lans = list()
for i in range(0,params.numberLans):
    lan = pg.LAN('lan-%d' % (i,))
    if params.trivialOk is True:
        lan.trivial_ok = True
    else:
        lan.trivial_ok = False
    if params.bestEffort is True:
        lan.best_effort = True
    else:
        lan.best_effort = False
    if params.multiplex is True:
        lan.link_multiplexing = True
    else:
        lan.link_multiplexing = False
    #if params.forceVlanLinkType:
    #    lan.type = "vlan"
    lan.vlan_tagging = True
    if params.linkSpeed > 0:
        lan.bandwidth = params.linkSpeed
    if params.linkLatency > 0:
        lan.latency = params.linkLatency
    #if params.linkLoss > 0:
    #    lan.loss = params.linkLoss
    lans.append(lan)
    pass

#if not params.hostImage:
#    if params.vmType == 'Docker':
#        params.hostImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//DOCKVH-CE17-1-27-STD'
#    elif params.vmType == 'Xen':
#        params.hostImage = 'urn:publicid:IDN+emulab.net+image+emulab-ops//XEN44-64-STD'
#    pass

vhosts = list()
vnodes = list()
vnodesByName = dict()
links = list()
lastSnakeLink = None
lastSnakeLinkNum = 0
imageList = []
sharedvlan = None
if params.vmImages:
    imageList = params.vmImages.split(" ")
    tmplist = []
    for x in imageList:
        if x and params.vmType == 'Docker' and params.dockerExternalImages:
            if x == 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU16-64-STD':
                x = 'ubuntu:16.04'
            elif x == 'urn:publicid:IDN+emulab.net+image+emulab-ops//CENTOS7-STD':
                x = 'centos:7'
        if x:
            tmplist.append(x)
        pass
    imageList = tmplist
    pass

for i in range(0,params.hosts):
    host = pg.RawPC('vhost-%d' % (i,))
    host.exclusive = True
    if params.hostType:
        host.hardware_type = params.hostType
    if params.hostImage:
        host.disk_image = params.hostImage
        pass
    if params.clientsideUpdate == "yes" or params.clientsideUpdate == "each":
        host.Attribute("CLIENTSIDE_UPDATE","1")
        if params.clientsideUpdate == "each":
            host.Attribute("CLIENTSIDE_UPDATE_EACHBOOT","1")
        if params.clientsideUpdateRepo:
            host.Attribute("CLIENTSIDE_UPDATE_REPO",params.clientsideUpdateRepo)
        if params.clientsideUpdateRef:
            host.Attribute("CLIENTSIDE_UPDATE_REF",params.clientsideUpdateRef)
    if params.storageDriver:
        host.Attribute("DOCKER_STORAGE_DRIVER",params.storageDriver)
        pass

    vhosts.append(host)

firstNodeDone = False
for i in range(-1,params.hosts):
    if i == -1 and params.hosts > 0:
        continue
    for j in range(0,params.vmsPerHost):
        node = None
        if i > -1:
            name = 'node-%d-%d' % (i,j)
        else:
            name = 'node-%d' % (j,)
        if params.vmType == 'Docker':
            node = ig.DockerContainer(name)
        else:
            node = ig.XenVM(name)
            if params.forceXenHVM:
                node.Attribute("XEN_FORCE_HVM","1")
            if params.forceXenPVH:
                node.Attribute("XEN_FORCE_PVH","1")
        if params.coresPerVM > 0:
            node.cores = params.coresPerVM
        if params.ramPerVM:
            node.ram = params.ramPerVM
        if params.hostType and params.vmType == 'Xen':
            node.xen_ptype = '%s-vm' % (params.hostType,)
        elif params.hostType and params.vmType == 'Docker':
            node.docker_ptype = '%s-vm' % (params.hostType,)
        if i > -1:
            node.InstantiateOn('vhost-%d' % (i,))
            node.exclusive = True
        else:
            node.exclusive = False
        if params.vmType == 'Xen' and len(imageList) > 0:
            node.disk_image = imageList[j % len(imageList)]
        elif params.vmType == 'Docker':
            if params.dockerExternalImages == True:
                node.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//DOCKER-EXT'
                if len(imageList) > 0:
                    extimage = imageList[j % len(imageList)]
                    eia = extimage.split("/",1)
                    if eia[0].find(".") > -1 or eia[0].find(":") > -1:
                        node.docker_extserver = eia[0]
                        node.docker_extimage = "/".join(eia[1:])
                    else:
                        node.docker_extimage = extimage
            elif params.dockerImageFromDockerfiles == True:
                node.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//DOCKER-EXT'
                if len(imageList) > 0:
                    node.docker_dockerfile = imageList[j % len(imageList)]
            elif len(imageList) > 0:
                node.disk_image = imageList[j % len(imageList)]
            if params.dockerTbAugmentation != 'default':
                node.docker_tbaugmentation = params.dockerTbAugmentation
            if params.dockerTbAugmentationUpdate:
                node.docker_tbaugmentation_update = params.dockerTbAugmentationUpdate
            if params.dockerSshStyle != 'default':
                node.docker_ssh_style = params.dockerSshStyle
            if params.dockerExecShell:
                node.docker_exec_shell = params.dockerExecShell
            pass
        if params.startupCmd:
            node.addService(pg.Execute(shell="sh",command=params.startupCmd))
        if params.tempBlockstoreMountPoint \
            and (len(tempBSNodes) == 0 or name in tempBSNodes):
            bs = node.Blockstore(
                name+"-temp-bs",params.tempBlockstoreMountPoint)
            bs.size = str(params.tempBlockstoreSize) + "GB"
            bs.placement = "any"
        if params.routableIPChoice == "first" and not firstNodeDone:
            node.routable_control_ip = True
        elif params.routableIPChoice == "all":
            node.routable_control_ip = True
        for k in range(0,params.numberLans):
            iface = node.addInterface('if%d' % (k,))
            lans[k].addInterface(iface)
            pass
        if params.buildSnakeLink:
            if not lastSnakeLink:
                lastSnakeLink = pg.Link("slink-%d" % (lastSnakeLinkNum,))
                if params.trivialOk:
                    lastSnakeLink.trivial_ok = True
                else:
                    lastSnakeLink.trivial_ok = False
                if params.bestEffort:
                    lastSnakeLink.best_effort = True
                if params.multiplex:
                    lastSnakeLink.link_multiplexing = True
                lastSnakeLink.vlan_tagging = True
                #if params.forceVlanLinkType:
                #    lastSnakeLink.type = "vlan"

                if params.linkSpeed > 0:
                    lastSnakeLink.bandwidth = params.linkSpeed
                if params.linkLatency > 0:
                    lastSnakeLink.latency = params.linkLatency
                #if params.linkLoss > 0:
                #    lastSnakeLink.loss = params.linkLoss

                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                lastSnakeLink.addInterface(siface)
            else:
                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                if params.trivialOk:
                    lastSnakeLink.trivial_ok = True
                else:
                    lastSnakeLink.trivial_ok = False
                if params.bestEffort:
                    lastSnakeLink.best_effort = True
                if params.multiplex:
                    lastSnakeLink.link_multiplexing = True
                lastSnakeLink.vlan_tagging = True
                #if params.forceVlanLinkType:
                #    lastSnakeLink.type = "vlan"
                if params.linkSpeed > 0:
                    lastSnakeLink.bandwidth = params.linkSpeed
                if params.linkLatency > 0:
                    lastSnakeLink.latency = params.linkLatency
                #if params.linkLoss > 0:
                #    lastSnakeLink.loss = params.linkLoss

                lastSnakeLink.addInterface(siface)
                links.append(lastSnakeLink)

                lastSnakeLinkNum += 1
                lastSnakeLink = pg.Link("slink-%d" % (lastSnakeLinkNum,))
                siface = node.addInterface('ifS%d' % (lastSnakeLinkNum,))
                lastSnakeLink.addInterface(siface)
            pass
        if params.dockerCmd:
            node.docker_cmd = params.dockerCmd
            #node.Attribute("DOCKER_CMD",params.dockerCmd)
        if params.dockerEnv:
            node.docker_env = params.dockerEnv
            #node.Attribute("DOCKER_ENV",params.dockerEnv)
        vnodes.append(node)
        vnodesByName[name] = node
        if not firstNodeDone and params.connectSharedVlan:
            iface = node.addInterface("ifSharedVlan")
            if sharedVlanAddress:
                iface.addAddress(
                    pg.IPv4Address(sharedVlanAddress,sharedVlanNetmask))
            sharedvlan = pg.Link('shared-vlan')
            sharedvlan.addInterface(iface)
            if params.createSharedVlan:
                sharedvlan.createSharedVlan(params.connectSharedVlan)
            else:
                sharedvlan.connectSharedVlan(params.connectSharedVlan)
            if params.multiplex:
                sharedvlan.link_multiplexing = True
                sharedvlan.best_effort = True
        if not firstNodeDone:
            firstNodeDone = True
        pass
    pass

#
# Add the blockstore, if requested.
#
bsnode = None
bslink = None
if params.blockstoreURN != "":
    if params.blockstoreMountNode \
        and not params.blockstoreMountNode in vnodesByName:
        #
        # This is a very late time to generate a warning, but that's ok!
        #
        perr = portal.ParameterError("The node on which you mount your remote dataset must exist, and does not!",
                                     ['blockstoreMountNode'])
        pc.reportError(perr)
        pc.verifyParameters()
        pass
    if params.blockstoreMountNode:
        rbsn = vnodesByName[params.blockstoreMountNode]
    else:
        rbsn = vnodes[0]
    myintf = rbsn.addInterface("ifbs0")
    
    bsnode = ig.RemoteBlockstore("bsnode",params.blockstoreMountPoint)
    bsintf = bsnode.interface
    bsnode.dataset = params.blockstoreURN
    #bsnode.size = params.N
    bsnode.readonly = params.blockstoreReadOnly
    
    bslink = pg.Link("bslink")
    bslink.addInterface(myintf)
    bslink.addInterface(bsintf)
    # Special blockstore attributes for this link.
    bslink.best_effort = True
    bslink.vlan_tagging = True
    links.append(bslink)
    pass

baseaddr = [10,0,0,0]
basebits = 8
minsubnetbits = basebits + 1
maxsubnetbits = 32 - 4
networks = dict()
netmasks = dict()
lastaddr = dict()
# The list of allowed subnet bit sizes.
ALLOWED_SUBNETS = []
# An ordered (increasing bit size) list of (bitsize,networkname) tuples.
REQUIRED_SUBNETS = []
# A dict of key networkname and value { 'bits':bitsize,'network':networkint,
#   'hostnum':hostint,'maxhosts':maxhostsint,'netmaskstr':'x.x.x.x' }
SUBNETS = {}

if params.ipAllocationUsesCIDR:
    subbits = basebits + 1
    while subbits <= maxsubnetbits:
        ALLOWED_SUBNETS.append(subbits)
        subbits += 1
else:
    ALLOWED_SUBNETS = [ 16,24 ]

def __htoa(h):
    return "%d.%d.%d.%d" % (
        (h >> 24) & 0xff,(h >> 16) & 0xff,(h >> 8) & 0xff,h & 0xff)

def ipassign_request_network(name,numhosts):
    #
    # Here, we just reserve each network's bitspace, in increasing
    # bitsize.  This makes eventual assignment trivial; we just pick the
    # next hole out of the previous (larger or equiv size) subnet.
    #
    _sz = numhosts + 1
    i = 0
    while _sz > 0:
        i += 1
        _sz = _sz >> 1
    # The minimum network size is a 255.255.255.252 (a /30).
    if i < 2:
        i = 2
    # Change the host bitsize to a network bitsize.
    i = 32 - i
    sn = None
    for _sn in ALLOWED_SUBNETS:
        if i < _sn:
            break
        sn = _sn
    if sn == None:
        raise Exception("network of bitsize %d (%d hosts) cannot be supported"
                        " with basebits %d" % (i,numhosts,basebits))
    ipos = 0
    for (bitsize,_name) in REQUIRED_SUBNETS:
        if sn < bitsize:
            break
        ipos += 1
    REQUIRED_SUBNETS.insert(ipos,(sn,name))
    return

def ipassign_assign_networks():
    basenum = baseaddr[0] << 24 | baseaddr[1] << 16 \
        | baseaddr[2] << 8 | baseaddr[3]
    # If any of our network broadcast addrs exceed this address, we've
    # overallocated.
    nextbasenum = basenum + 2 ** (32 - basebits)

    #
    # Process REQUIRED_SUBNETS list into SUBNETS dict, and setup the
    # next host number.  Again, we assume that REQUIRED_SUBNETS is
    # ordered in increasing number of network bits required.
    #
    lastbitsize = None
    lastnetnum = None
    for i in range(0,len(REQUIRED_SUBNETS)):
        (bitsize,name) = REQUIRED_SUBNETS[i]
        if lastbitsize != bitsize and lastbitsize != None:
            # Need to get into the next lastbitsize subnet to allocate
            # for the bitsize subnet.
            lastnetnum += 2 ** (32 - lastbitsize)

        if lastnetnum != None:
            netnum = lastnetnum + 2 ** (32 - bitsize)
        else:
            netnum = basenum
        if netnum >= nextbasenum:
            raise Exception("out of network space in /%d at network %s (/%d)"
                            % (basebits,name,bitsize))
        bcastnum = netnum + (2 ** (32 - bitsize)) - 1
        netmask = 0
        for i in range(32 - bitsize,32):
            netmask |= (1 << i)
        SUBNETS[name] = {
            'bits':bitsize,'netnum':netnum,
            'networknum':netnum,'networkstr':__htoa(netnum),
            'bcastnum':bcastnum,'bcaststr':__htoa(bcastnum),
            'hostnum':0,'maxhosts':2 ** (32 - bitsize) - 1,'addrs':[],
            'netmask':netmask,'netmaskstr':__htoa(netmask)
        }
        lastnetnum = netnum
        lastbitsize = bitsize

def ipassign_assign_host(lan):
    #
    # Assign an IP address in the given lan.  If you call this function
    # more times than you effectively promised (by calling
    # ipassign_add_network with a requested host size), you will be
    # handed an Exception.
    #
    bitsize = None
    for (_bitsize,_name) in REQUIRED_SUBNETS:
        if _name == lan:
            bitsize = _bitsize
    if not bitsize:
        raise Exception("unknown network name %s" % (lan,))
    if not _name in SUBNETS:
        raise Exception("unknown network name %s" % (lan,))

    maxhosts = (2 ** bitsize - 1)
    if SUBNETS[lan]['hostnum'] >= maxhosts:
        raise Exception("no host space left in network name %s (max hosts %d)"
                        % (lan,maxhosts))

    SUBNETS[lan]['hostnum'] += 1

    addr = __htoa(SUBNETS[lan]['netnum'] + SUBNETS[lan]['hostnum'])
    if True:
        SUBNETS[lan]['addrs'].append(addr)
    return (addr,SUBNETS[lan]['netmaskstr'])

#
# Assign IP addresses if necessary.
#
if params.ipAllocationStrategy == 'script':
    for lan in lans:
        name = lan.client_id
        ipassign_request_network(name,len(lan.interfaces))
    for link in links:
        name = link.client_id
        ipassign_request_network(name,len(link.interfaces))
    ipassign_assign_networks()
    for lan in lans:
        name = lan.client_id
        for iface in lan.interfaces:
            (address,netmask) = ipassign_assign_host(name)
            iface.addAddress(pg.IPv4Address(address,netmask))
    for link in links:
        name = link.client_id
        for iface in link.interfaces:
            (address,netmask) = ipassign_assign_host(name)
            iface.addAddress(pg.IPv4Address(address,netmask))
    pass

#
# If this is a LAN-only experiment, turn off ddjikstra routing; we don't
# need it, and it explodes on huge LANs (i.e., on a 5k topo, 30s per vnode).
#
if not params.buildSnakeLink:
    request.setRoutingStyle("none")

for x in vhosts:
    request.addResource(x)
for x in vnodes:
    request.addResource(x)
for x in lans:
    request.addResource(x)
for x in links:
    request.addResource(x)
if bsnode:
    request.addResource(bsnode)
if sharedvlan:
    request.addResource(sharedvlan)

pc.printRequestRSpec(request)
